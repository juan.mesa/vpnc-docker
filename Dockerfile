FROM ubuntu:16.04

# Install vpnc and some tools to debug / check vpn
# connection.

RUN apt-get update  \
    && apt-get install socat -y  \
    && apt-get install vpnc -y \
    && apt-get install wget -y  \
    && apt-get install iputils-ping -y  \
    && apt-get install net-tools -y  \
    && apt-get install gcc -y

# Copy start and proxy scripts
COPY start.sh /

RUN chmod +x start.sh

CMD ./start.sh
