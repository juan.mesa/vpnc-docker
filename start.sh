#!/bin/bash

connect () {
  [[ -z "$1" ]] && echo "*** Connecting to VPN..." || echo '*** Reconnecting to VPN...'
  vpnc-connect
  echo "*** Remove the new gateway, and put the old into place"
  route del default dev tun0
  route add -net $REMOTE_NETWORK netmask $REMOTE_NETMASK dev tun0
  route add default gw $gateway_ip dev eth0
}

test_connection(){
  count=$( ping -c 3 $ENDPOINT | grep icmp* | wc -l )
  if [ $count -eq 0 ]
      then
      # Ping failed
      echo "[ERROR] Ping FAILED $(date) for $ENDPOINT"
      connect reconection
  else
    echo "[OK] Ping replied $(date) from $ENDPOINT"
  fi
}

start_socat () {
  port=$1
  while true; do
    socat UDP4-RECVFROM:161,reuseaddr UDP4-SENDTO:${ENDPOINT}:${port}
  done
}

echo "*** Getting current default gw"
gateway_ip=$(ip -4 route list 0/0 | cut -d ' ' -f 3)
connect

echo '*** Starting forwarder/proxy..'
start_socat 161 &
start_socat 162 &

while true
do
  [[ $RESTART == 1 ]] && test_connection
  sleep 30
done
